main_document = LatexEnvironment

CIPATH=$(shell git rev-parse --path-format=relative --show-toplevel)/ci

TEXMFHOME=$(CIPATH)/latextemplates

include $(CIPATH)/CITemplates/Latex/buildLatex.mk
